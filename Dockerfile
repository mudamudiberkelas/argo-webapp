FROM scratch

LABEL maintaner="Andrzej Kaczynski <mudamudiberkelas@gmail.com>"

COPY . .

EXPOSE 8080

CMD ["./main"]
